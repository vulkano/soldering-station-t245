#ifndef PCF8574_H
#define PCF8574_H

#include <stdint.h>
#include "types.h"

#define PCF8574_I2C_ADDRESS 0x27u

t_Result pcf8574_write(const uint8_t data);
t_Result pcf8574_multiple_write(const uint8_t* data, const uint8_t data_length);

#endif //PCF8574_H
