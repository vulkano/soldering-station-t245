#include <stdlib.h>
#include "pcf8574.h"
#include "i2c.h"

t_Result pcf8574_write(const uint8_t data)
{
    t_Result result = ERROR;

    result = i2c_transmit_byte(PCF8574_I2C_ADDRESS, data);

    return result;
}

t_Result pcf8574_multiple_write(const uint8_t* data, const uint8_t data_length)
{
    t_Result result = ERROR;

    if (data != NULL)
    {
        result = i2c_transmit_nbytes(PCF8574_I2C_ADDRESS, data, data_length);
    }

    return result;
}
