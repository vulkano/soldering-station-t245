#ifndef I2C_H
#define I2C_H

#include <stdint.h>
#include "../../includes/types.h"

#define MAX_SCK_CLOCK_IN_HZ 400000U // 400kHz

typedef enum
{
    START = 0x08,
    MT_SLA_ACK = 0x18,
    MT_SLA_NO_ACK = 0x20,
    MT_DATA_ACK = 0x28,
    MT_DATA_NO_ACK = 0x30,
} t_I2cStatusCodes;

void i2c_set_sck_clock(const uint32_t sck_clock);
void i2c_init(const uint32_t sck_clock_in_hz);
t_Result i2c_transmit_byte(const uint8_t slave_address, const uint8_t data);
t_Result i2c_transmit_nbytes(const uint8_t slave_address, const uint8_t* data, const uint8_t nbytes);

#endif /* I2C_H */
