#include <stdlib.h>
#include <avr/io.h>
#include "i2c.h"
#include "registers.h"
#include "gpios.h"
#include <avr/interrupt.h>


static inline void i2c_send_start_condition()
{
    TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);
}

static inline void i2c_send_stop_condition()
{
    TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO);
}

static inline void i2c_send(const uint8_t data)
{
    TWDR = data;
    TWCR = (1<<TWINT) | (1<<TWEN);
}

/**
  * The SCL frequency is generated according to the following equation:
  *     scl_frequency = CPU_CLOCK / (16 + 2 * TWRB * prescaler_value)
  * Where:
  *     TWRB is value of the TWI Bit Rate Register
*/
void i2c_set_sck_clock(const uint32_t sck_clock_in_hz)
{
    TWBR = (uint8_t)(((F_CPU / sck_clock_in_hz) - 16) / 2);
}

void i2c_init(const uint32_t sck_clock_in_hz)
{
    gpio_set_dir(PORT_C, PIN_4, IN_PULLUP);
    gpio_set_dir(PORT_C, PIN_5, IN_PULLUP);

    CLR_REG_BIT(TWSR, TWPS0);
    CLR_REG_BIT(TWSR, TWPS1);

    i2c_set_sck_clock(sck_clock_in_hz);

    TWCR = (1<<TWINT) | (1<<TWEN);
}

static void i2c_wait_twint()
{
    while (!(TWCR & (1<<TWINT)));
}

t_Result i2c_transmit_byte(const uint8_t slave_address, const uint8_t data)
{
    t_Result result = ERROR;

    if (!(slave_address & 0x80))
    {
        i2c_send_start_condition();
        i2c_wait_twint();
        i2c_send((uint8_t)(slave_address << 1));
        i2c_wait_twint();
        i2c_send(data);
        i2c_wait_twint();
        i2c_send_stop_condition();
        while ((TWCR & (1<<TWSTO)));

        result = OK;
    }
    else
    {
        result = ADDRESS_OUT_OF_RANGE;
    }

    return result;
}

t_Result i2c_transmit_nbytes(const uint8_t slave_address, const uint8_t* data, const uint8_t nbytes)
{
    t_Result result = OK;

    if (data != NULL)
    {
        for (uint8_t i = 0; (i < nbytes) && (result == OK); i++)
        {
            result = i2c_transmit_byte(slave_address, data[i]);
        }

    }

    return result;
}

