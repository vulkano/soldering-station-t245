#ifndef UART_H
#define UART_H

#include <stdint.h>

#ifndef F_CPU
# warning "F_CPU not defined for <uart.h>"
#endif

#define UART_BUFFER_SIZE 16u
#define UBBR_VALUE 8u //equivalent to baud rate of 115200

void uart_init();
void uart_put_char(char c);
void uart_put_string(char* str);

#endif /* UARTH_H */