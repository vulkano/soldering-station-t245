#ifndef ADC_H
#define ADC_H

#include <stdint.h>
#include "registers.h"

#define ADC_SAMPLE_BUFFER_MAX_SIZE 20u
#define ADC_RESOLUTION_IN_BITS 10u
#define ADC_REF_VOLT 5.0f
#define MAX_PRESCALER_CODE 7u

typedef enum
{
    NOT_FULL = 0,
    FULL = 1
}t_AdcBufferStatus;

typedef enum
{
    FREE_RUNNING = 0,
    SINGLE = 1
}t_AdcSamplingMode;

typedef enum
{
    AREF = 0,
    AVCC = 1,
    INTERNAL_1V1 = 3
}t_AdcReferenceVoltage;

typedef struct
{
    uint8_t num_of_samples;
    reg8_t sample_counter;
    reg16_t* buff;
    volatile t_AdcBufferStatus status;
}t_AdcSampleBuffer;

typedef struct
{
    uint8_t sample_buffer_max_size;
    uint16_t reference_voltage_mv;
    t_AdcSamplingMode adc_mode;
    t_AdcSampleBuffer sample_buffer;
}t_AdcContext;

void adc_init(t_AdcReferenceVoltage ref_voltage, uint16_t ref_volt_mv);
uint16_t adc_read(void);
void adc_set_reference(t_AdcReferenceVoltage ref_voltage, uint16_t ref_volt_mv);
t_AdcReferenceVoltage adc_get_reference(void);
volatile uint16_t* adc_get_samples(uint8_t number_of_samples);
void adc_enable(void);
void adc_enable_interrupt(void);
void adc_disable_interrupt(void);
void adc_start_conversion(void);
void adc_enable_auto_triggering(void);
void adc_disable_auto_triggering(void);

#endif /* ADC_H */
