#ifndef PID_H
#define PID_H

#include <stdint.h>

// Sampling Time Interval
#define PID_INTERVAL_IN_MS 20

#define SCALING_FACTOR 128

// P, I and D parameter values
// The K_P, K_I and K_D values (P, I and D gains)
// need to be modified to adapt to the application at hand
#define K_P 1.0
#define K_D 0.0
#define K_I 0.0

typedef struct PID_DATA {
	int16_t lastProcessValue;
	int32_t sumError;
	int16_t P_Factor;
	int16_t I_Factor;
	int16_t D_Factor;
	int16_t maxError;
	int32_t maxSumError;
} t_PidData;

extern t_PidData  g_pidData;

#define MAX_INT INT16_MAX
#define MAX_LONG INT32_MAX
#define MAX_I_TERM (MAX_LONG / 2)

#define FALSE 0
#define TRUE 1

void    pid_init(int16_t p_factor, int16_t i_factor, int16_t d_factor, t_PidData* pid);
int16_t pid_controller(int16_t setPoint, int16_t processValue, t_PidData* pid_st);
void    pid_reset_integrator(t_PidData* pid_st);

#endif // PID_H
