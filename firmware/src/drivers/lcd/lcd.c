#include <stdlib.h>
#include <util/delay.h>
#include "lcd.h"
#include "util/delay.h"
#include "pcf8574.h"

static t_Result lcd_write(const uint8_t data)
{
    pcf8574_write(data | (1 << ENABLE));
    _delay_us(ENABLE_PULSE_DURATION_IN_US);
    pcf8574_write(data);
    _delay_us(FRAME_DELAY_IN_US); // wait for more than 100us
    return OK;
}

static t_Result lcd_init_4bit_data_line(void)
{
    const uint8_t init_4bit_instruction = 0b00110000;

    // Consult page 46, Figure 24 of Hitachi HD44780U manual for more details
    _delay_ms(POWERUP_DELAY_IN_MS); // wait for Vcc to rise over 2.7v
    lcd_write(init_4bit_instruction);
    _delay_ms(FOUR_BIT_MODE_INIT_DELAY_IN_MS); // wait for more than 4.1ms
    lcd_write(init_4bit_instruction);
    _delay_ms(FOUR_BIT_MODE_INIT_DELAY_IN_MS); // wait for more than 4.1ms
    lcd_write(init_4bit_instruction);
    _delay_us(FRAME_DELAY_IN_US); // wait for more than 100us
    lcd_write(0b00100000);

    return OK;
}

t_Result lcd_send_command(const uint8_t data, const t_RegisterSelect reg)
{
    uint8_t high_nibble = (uint8_t)((data & 0xF0u) | (reg << RS) | (BACK_LIGHT_ON << BL));
    uint8_t low_nibble = (uint8_t)((data << 4) | (reg << RS) | (BACK_LIGHT_ON << BL));

    lcd_write(high_nibble);
    lcd_write(low_nibble);

    return OK;
}

t_Result lcd_print(const char* str)
{
    t_Result result = ERROR;
    uint8_t cnt = LCD_COLUMNS;

    if (str != NULL)
    {
        while((*str) && (cnt--))
        {
            result = lcd_send_command(*str++, DATA_REGISTER);
        }
    }

    return result;
}

t_Result lcd_clear_display(void)
{
    t_Result result = ERROR;

    result = lcd_send_command(INSTR_CLEAR_DISPLAY, INSTRUCTION_REGISTER); 
    _delay_ms(CLEAR_AND_HOME_DELAY_IN_MS);

    return result;
}

t_Result lcd_return_home(void)
{
    t_Result result = ERROR;

    result = lcd_send_command(INSTR_RETURN_HOME, INSTRUCTION_REGISTER); 
    _delay_ms(CLEAR_AND_HOME_DELAY_IN_MS);

    return result;
}

t_Result lcd_set_cursor(const uint8_t row, const uint8_t column)
{
    t_Result result = ERROR;
    uint8_t address = 0;

    if ((row < LCD_ROWS) && (column < LCD_COLUMNS))
    {
        address = row * SECOND_LINE_START_ADDRESS + column;
        lcd_send_command(INSTR_SET_DDRAM_ADDRESS(address), INSTRUCTION_REGISTER);
    }

    return result;
}

void lcd_init(void)
{
    lcd_init_4bit_data_line();
    lcd_send_command(INSTR_FUNCTION_SET_4BIT_2LINE_8x5FONT, INSTRUCTION_REGISTER);
    lcd_send_command(INSTR_DISPLAY_ON_NOCURSOR_NOBLINKING, INSTRUCTION_REGISTER);
    lcd_send_command(INSTR_ENTRY_MODE_SET_INCREMENT, INSTRUCTION_REGISTER);
    lcd_clear_display();
}
