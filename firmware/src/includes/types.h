#ifndef TYPES_H
#define TYPES_H

typedef enum
{
    OK = 0,
    ERROR = 1,
    ADDRESS_OUT_OF_RANGE = 2,
    DATA_EXCEED_BUFFER_SIZE = 3,
} t_Result;

#endif //TYPES_H