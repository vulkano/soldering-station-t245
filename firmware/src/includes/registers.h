#ifndef REGISTERS_H
#define REGISTERS_H

#include <stdint.h>


/*volatile status register*/
typedef volatile uint8_t sreg8_t;
typedef volatile uint16_t sreg16_t;
typedef volatile uint32_t sreg32_t;

/*volatile data register*/
typedef volatile uint8_t reg8_t;
typedef volatile uint16_t reg16_t;
typedef volatile uint32_t reg32_t;

#define SET_REG_BIT(REG, BIT) ((REG) |= (uint8_t)(1 << (BIT)))
#define CLR_REG_BIT(REG, BIT) ((REG) &= (uint8_t)~(1 << (BIT)))
#define TOGGLE_REG_BIT(REG, BIT) ((REG) ^= (uint8_t)(1 << (BIT)))

#endif /* REGISTERS_H */