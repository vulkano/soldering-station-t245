#include <avr/io.h>
#include <avr/interrupt.h>
#include "encoder.h"
#include "gpios.h"
#include "timer.h"
#include "stdint.h"

static t_EncoderContext encoder_context;

static t_EdgeCode encoder_get_edge_(uint8_t intx);

t_EncoderSwState encoder_get_switch_state(void)
{
    t_EncoderSwState state = IDLE;

    encoder_context.sw_status.b.level = gpio_read(PORT_D, PIN_4);

    if (((encoder_context.sw_status.b.old_level == HIGH) || (encoder_context.sw_status.b.old_level == LOW)) && (encoder_context.sw_status.b.level == LOW))
    {
        if (encoder_context.sw_status.b.pushed)
        {
            if ((timer_get_millis() - encoder_context.time) > SW_HOLD_TIME)
            {
                encoder_context.sw_status.b.pushed = 0;
                if(!encoder_context.sw_status.b.hold)
                {
                    encoder_context.sw_status.b.hold = 1;
                    state = HOLD;
                }
            }
        }
        else
        {
            encoder_context.sw_status.b.pushed = 1;
            encoder_context.time = timer_get_millis();
        }
    }
    else if ((encoder_context.sw_status.b.old_level == LOW) && (encoder_context.sw_status.b.level == HIGH) && encoder_context.sw_status.b.pushed)
    {
        encoder_context.sw_status.b.pushed = 0;
        if(encoder_context.sw_status.b.hold)
        {
            encoder_context.sw_status.b.hold = 0;
        }
        else
        {
            state = PRESS;
        }
    }

    encoder_context.sw_status.b.old_level = encoder_context.sw_status.b.level;

    return state;
}

ISR(INT0_vect)
{
    EIMSK &= ~(1 << INT0); // disable INT0 interrupt

    if (encoder_get_edge_(INT0) == FALLING)
    {
        encoder_context.rot_status.b.a_falling = 1;
    }
    else if (encoder_get_edge_(INT0) == RISING)
    {
        if (encoder_context.rot_status.b.a_falling && encoder_context.rot_status.b.b_falling && encoder_context.rot_status.b.b_rising)
        {
            encoder_context.rot_status.w = 0;
            if (encoder_context.value < INT16_MAX)
            {
                encoder_context.value += ENCODER_DIRECTION;
            }
        }
        else if (encoder_context.rot_status.b.a_falling && encoder_context.rot_status.b.b_falling)
        {
            encoder_context.rot_status.b.a_rising = 1;
        }
        else
        {
            encoder_context.rot_status.w = 0;
        }
    }

    EICRA ^= (1 << ISC00); // toggle INT0 triggering edge
    EIMSK |= (1 << INT0); // enable INT0 interrupt
}

ISR(INT1_vect)
{
    EIMSK &= ~(1 << INT1); // disable INT1 interrupt

    if (encoder_get_edge_(INT1) == FALLING)
    {
        encoder_context.rot_status.b.b_falling = 1;
    }
    else if (encoder_get_edge_(INT1) == RISING)
    {
        if (encoder_context.rot_status.b.b_falling && encoder_context.rot_status.b.a_falling && encoder_context.rot_status.b.a_rising)
        {
            encoder_context.rot_status.w = 0;
            if (encoder_context.value > INT16_MIN)
            {
                encoder_context.value -= ENCODER_DIRECTION;
            }
        }
        else if (encoder_context.rot_status.b.b_falling && encoder_context.rot_status.b.a_falling)
        {
            encoder_context.rot_status.b.b_rising = 1;
        }
        else
        {
            encoder_context.rot_status.w = 0;
        }
    }

    EICRA ^= (1 << ISC10); // toggle INT1 triggering edge
    EIMSK |= (1 << INT1); // enable INT1 interrupt
}

int16_t encoder_get_value(void)
{
    int16_t value = encoder_context.value;
    return value;
}

static t_EdgeCode encoder_get_edge_(uint8_t intx)
{
    if (intx == INT0)
    {
        if ((EICRA >> ISC00) & 1)
        {
            return RISING;
        }
        else
        {
            return FALLING;
        }
    }
    else if(intx == INT1)
    {
        if ((EICRA >> ISC10) & 1)
        {
            return RISING;
        }
        else
        {
            return FALLING;
        }
    }

    return NONE;
}

void encoder_init(void)
{
    gpio_set_dir(ENC_PORT, ENC_PIN_A, IN_PULLUP);
    gpio_set_dir(ENC_PORT, ENC_PIN_B, IN_PULLUP);
    gpio_set_dir(ENC_PORT, ENC_PIN_SW, IN_PULLUP);
    EICRA |= (1 << ISC01); // falling edge of INT0 generates an interrupt request
    EICRA |= (1 << ISC11); // falling edge of INT1 generates an interrupt request
    EIMSK |= (1 << INT0); // enable INT0 interrupt
    EIMSK |= (1 << INT1); // enable INT1 interrupt
}
