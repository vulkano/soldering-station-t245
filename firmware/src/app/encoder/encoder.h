#ifndef ENCODER_H
#define ENCODER_H

#include <stdint.h>
#include "registers.h"

#define ENCODER_DIRECTION 1; // change to -(ENCODER_DIRECTION) in order to change direction
#define SW_HOLD_TIME 3000u // 3000 milliseconds = 3 seconds

//Configure encoder pins: A<->D2, B<->D3, SW<->D4
#define ENC_PORT PORT_D
#define ENC_PIN_A PIN_2
#define ENC_PIN_B PIN_3
#define ENC_PIN_SW PIN_4

typedef enum
{
    IDLE = 0,
    PRESS = 1,
    HOLD = 2
} t_EncoderSwState;

typedef enum
{
    FALLING = 0,
    RISING = 1,
    NONE = 2
} t_EdgeCode;

typedef struct
{
    sreg32_t time;
    int16_t value;
    union
    {
        struct
        {
            sreg8_t a_falling:1;
            sreg8_t a_rising:1;
            sreg8_t b_falling:1;
            sreg8_t b_rising:1;
        } b;

        sreg8_t w;
    } rot_status;

    union
    {
        struct
        {
            sreg8_t falling:1;
            sreg8_t rising:1;
            sreg8_t pressed:1;
            sreg8_t level:1;
            sreg8_t old_level:1;
            sreg8_t hold:1;
            sreg8_t pushed:1;
        } b;
        sreg8_t w;
    } sw_status;
} t_EncoderContext;

int16_t encoder_get_value(void);
void encoder_init(void);
t_EncoderSwState encoder_get_switch_state(void);

#endif /* ENCODER_H */
