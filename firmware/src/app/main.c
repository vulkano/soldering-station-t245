#include "app.h"
#include "init.h"

int main()
{
    init_peripherals();

    while(1)
    {
        app_run();
    }
    return 0;
}
