#include <avr/io.h>
#include "gpios.h"

t_GpioDir gpio_get_dir(t_GpioPort port , t_GpioPin pin)
{
     t_GpioDir pin_dir = 0;

    if (pin > PIN_7 || ((port == PORT_C) && (pin > PIN_6)))
    {
        return EGPIO;
    }

    switch(port)
    {
        case PORT_B:
            pin_dir  = (DDRB >> pin) & 0x1u;
            if (pin_dir == IN)
            {
                if ((PORTB >> pin) & 0x1u)
                pin_dir = IN_PULLUP;
            }
            break;
        case PORT_C:
            pin_dir = (DDRC >> pin) & 0x1u;
            if (pin_dir == IN)
            {
                if ((PORTC >> pin) & 0x1u)
                pin_dir = IN_PULLUP;
            }
            break;
        case PORT_D:
            pin_dir = (DDRD >> pin) & 0x1u;
            if (pin_dir == IN)
            {
                if ((PORTD >> pin) & 0x1u)
                pin_dir = IN_PULLUP;
            }
            break;
        default:
            pin_dir = EGPIO;
    }

    return pin_dir;
}

void gpio_set_dir(t_GpioPort port , t_GpioPin pin, t_GpioDir dir)
{
    if (pin > PIN_7 || ((port == PORT_C) && (pin > PIN_6)))
    {
        return;
    }

    if (port == PORT_B)
    {
        switch(dir)
        {
            case OUT:
                DDRB |= (1 << pin);
                PORTB &= ~(1 << pin);
                break;
            case IN:
                DDRB &= ~(1 << pin);
                PORTB &= ~(1 << pin);
                break;
            case IN_PULLUP:
                DDRB &= ~(1 << pin);
                PORTB |= (1 << pin);
                break;
            default:
                return;
        }
    }
    else if (port == PORT_D)
    {
                switch(dir)
        {
            case OUT:
                DDRD |= (1 << pin);
                PORTD &= ~(1 << pin);
                break;
            case IN:
                DDRD &= ~(1 << pin);
                PORTD &= ~(1 << pin);
                break;
            case IN_PULLUP:
                DDRD &= ~(1 << pin);
                PORTD |= (1 << pin);
                break;
            default:
                return;
        }
    }
    else if (port == PORT_C)
    {
        switch(dir)
        {
            case OUT:
                DDRC |= (1 << pin);
                PORTC &= ~(1 << pin);
                break;
            case IN:
                DDRC &= ~(1 << pin);
                PORTC &= ~(1 << pin);
                break;
            case IN_PULLUP:
                DDRC &= ~(1 << pin);
                PORTC |= (1 << pin);
                break;
            default:
                return;
        }
    }
}

void gpio_write(t_GpioPort port , t_GpioPin pin, t_GpioLevel level)
{
    if (pin > PIN_7 || ((port == PORT_C) && (pin > PIN_6)))
    {
        return;
    }

    switch(port)
    {
        case PORT_B:
            PORTB = level ? PORTB | (1 << pin) : PORTB & ~(1 << pin);
            break;
        case PORT_C:
            PORTC = level ? PORTB | (1 << pin) : PORTB & ~(1 << pin);
            break;
        case PORT_D:
            PORTD = level ? PORTB | (1 << pin) : PORTB & ~(1 << pin);
            break;
        default:
            return;
    }
}

t_GpioLevel gpio_read(t_GpioPort port , t_GpioPin pin)
{
    t_GpioLevel pin_level = 0;

    if (pin > PIN_7 || ((port == PORT_C) && (pin > PIN_6)))
    {
        return EGPIO;
    }

    switch(port)
    {
        case PORT_B:
            pin_level  = (PINB >> pin) & 0x1u;
            break;
        case PORT_C:
            pin_level = (PINC >> pin) & 0x1u;
            break;
        case PORT_D:
            pin_level = (PIND >> pin) & 0x1u;
            break;
        default:
            pin_level = EGPIO;
    }

    return pin_level;
}

void gpio_toggle(t_GpioPort port , t_GpioPin pin)
{
    if (pin > PIN_7 || ((port == PORT_C) && (pin > PIN_6)))
    {
        return;
    }

    switch(port)
    {
        case PORT_B:
            PINB |= (1 << pin);
            break;
        case PORT_C:
            PINC |= (1 << pin);
            break;
        case PORT_D:
            PIND |= (1 << pin);
            break;
        default:
            return;
    }
}