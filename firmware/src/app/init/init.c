#include "heater.h"
#include "uart.h"
#include "i2c.h"
#include "adc.h"
#include "lcd.h"
#include "encoder.h"
#include "timer.h"
#include "pcf8574.h"
#include "pid.h"

static char uart_greeting_message[] = \
"**********************************\n\
* Digital Soldering Station Unit *\n\
**********************************\n\
\n\
Source code: https://codeberg.org/vulkano/soldering-station-t245\n\
Author: Matija Radovic\n\
=================================================================\n\n";

void init_peripherals(void)
{
    cli(); // Disable global interrupts
    uart_init();
    i2c_init(MAX_SCK_CLOCK_IN_HZ);
    adc_init(AVCC, 5000);
    encoder_init();
    timer_init(TIMER0);
    heater_init();
	pid_init(K_P * SCALING_FACTOR, K_I * SCALING_FACTOR, K_D * SCALING_FACTOR, &g_pidData);
    pcf8574_write(0);
    lcd_init();
    sei(); // Enable global interrupts
    uart_put_string(uart_greeting_message);
}
