#include <stdint.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "timer.h"
#include "registers.h"

static volatile uint32_t milliseconds;
static volatile uint32_t microseconds;

ISR(TIMER0_COMPA_vect)
{
    milliseconds++;
}

uint32_t timer_get_millis(void)
{
    return milliseconds;
}

uint32_t timer_get_micros(void)
{
    return microseconds;
}

void delay_ms(uint32_t millis)
{
    if (millis == 0)
    {
        return;
    }

    while(millis--)
    {
        _delay_ms(1);
    }
}

void delay_us(uint32_t micros)
{
    if (micros == 0)
    {
        return;
    }

    while(micros--)
    {
        _delay_us(1);
    }
}

void timer_init(t_TimerId timer_id)
{
    switch(timer_id)
    {
        case TIMER0:
            SET_REG_BIT(TCCR0B,CS00);
            SET_REG_BIT(TCCR0B,CS01); // system clock with prescaler value of 64
            SET_REG_BIT(TCCR0A,WGM01); // set CTC mode while OC0A is disconnected
            OCR0A = 250;
            SET_REG_BIT(TIMSK0,OCIE0A); // enable timer/counter0 compare match A interrupt
            break;
        default:
            break;

    }
}
