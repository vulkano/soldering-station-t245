#ifndef TIME_H
#define TIME_H

#include <stdint.h>
#include <avr/interrupt.h>

#define MICROS_RESOLUTION 200;

typedef enum
{
    TIMER0 = 0,
    TIMER1 = 1
}t_TimerId;

uint32_t timer_get_millis(void);
void timer_init(t_TimerId timer_id);
void delay_ms(uint32_t milliseconds);
void delay_us(uint32_t microseconds);

#endif /* TIME_H */
