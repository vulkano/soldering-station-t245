#ifndef _APP_H_
#define _APP_H_

void app_run(void);
void app_display_update(void);

#endif // _APP_H_
